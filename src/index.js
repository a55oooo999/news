import React from 'react';
import {HashRouter as Router, Route, Switch } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Groom from '@/components/groom/Groom';
import Sun from '@/components/sun/Sun';
import HotSpot from '@/components/hotspot/HotSpot';
import Photo from '@/components/photo/Photo';
import App from '@/App';
import '@/main.scss';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Router>
        <Switch>
            <Route path='/groom' component = { Groom } />
            <Route path='/sun' component = { Sun } />
            <Route path='/hotspot' component = { HotSpot } />
            <Route path='/photo' component = { Photo } />
            <Route path='/' component = { App } />
        </Switch>
    </Router>, 
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
process.env.NODE_ENV === 'production' ? serviceWorker.register() : serviceWorker.unregister()

