import React, { Component } from 'react';
import GlobalNav from '@/components/common/globalnav/GlobalNav';
import TopBar from '@/components/common/topbar/TopBar';
import MainCenter from './maincenter/MainCenter';
class App extends Component {
  render() {
    return (
        <div className="container">
          <div className="header">
           <TopBar/>
          </div>
          <div className="main">
            <div className="main-left">
            <GlobalNav />
            </div>
            <div className="main-center">
            <MainCenter />
            </div>
            <div className="main-right"></div>
          </div>
        </div>
      );
  }
}

export default App;
