import React, { Component } from 'react';
import './maincenter.scss';
import MainCenterTop from './maincentertop/MainCenterTop';
import MainCenterBottom from './maincenterbottom/MainCenterBottom';
class App extends Component {
  render() {
    return (
          <div className="groom-main-center">
            <MainCenterTop />
            <MainCenterBottom />
          </div>
      );
  }
}

export default App;
