import React, { Component } from 'react';
import './question.scss';
class App extends Component {
  render() {
    return (
            <div className="groom-main-center-top-question">
             <div className="question-box">
             <input type="text" placeholder="请输入问题标题（4-40字）" className="question-box-top1" />
                <textarea className="question-box-top2" placeholder="添加问题背景描述（选填，0-40字）"></textarea>
                <p className="question-box-word">0/40字</p>
                <div className="question-box-bottom">
                  <ul className="question-box-bottom-ul">
                    <li><i className="iconfont icon-tupian"></i><span>添加图片</span></li>
                    <li><i className="iconfont icon-wenzhang1"></i><span>更多问答</span></li>
                    <li className="question-box-bottom-ul-lilast"><b>发布</b></li>
                  </ul>
                </div>
            </div>
            </div>
      );
  }
}

export default App;
