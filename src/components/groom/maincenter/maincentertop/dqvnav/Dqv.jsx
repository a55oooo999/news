import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './dqv.scss';
class App extends Component {
  componentDidMount () {
    console.log(this.props.list)
  }
  render() {
    return (
             <ul className="main-center-top-box-ul">
               {
                  this.props.list.map((item,index)=>{
                    return(
                      <li key={index}>
                        <NavLink to={item.path}>{item.name}</NavLink>
                      </li>
                    )
                  })
                }
             </ul>
          )
      }
}

export default App;
