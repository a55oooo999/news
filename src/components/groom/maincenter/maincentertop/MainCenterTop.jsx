import React, { Component } from 'react';
import { Route, Switch, Redirect} from 'react-router-dom';
import Default from './default/Default';
import Question from './question/Question';
import Video from './video/Video';
import Dqv from './dqvnav/Dqv';
import './maincentertop.scss';
class App extends Component {
  constructor(props) {
    super(props);
    this.state={
      list:[
        {
          path: '/groom/default',
          name: '发布图文',
        },
        {
          path: '/groom/video',
          name: '发布视频',
        },
        {
          path: '/groom/question',
          name: '发布问答',
        }
      ]
    }
  }
  render() {
    return (
            <div className="groom-main-center-top">
            <Dqv list = { this.state.list } />
             <div className="groom-main-center-middle">
        <Switch>
          <Route path="/groom/question" component = { Question } />
          <Route path="/groom/video" component = { Video } />
          <Route path="/groom/default" component = { Default } />
          <Redirect from='/groom' to='/groom/default'/>
        </Switch>
          </div>
        </div>
      );
  }
}

export default App;
