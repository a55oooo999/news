import React, { Component } from 'react';
import './video.scss';
class App extends Component {
  render() {
    return (
            <div className="groom-main-center-top-video">
             <div className="video-box">
                <textarea className="video-box-top" placeholder="视频标题（30 字以内）"></textarea>
                <p className="video-box-word">0/30字</p>
                <div className="video-box-bottom">
                  <ul className="video-box-bottom-ul">
                    <li><i className="iconfont icon-ziyuan"></i><span>添加视频</span></li>
                    <li className="video-box-bottom-ul-lilast"><b>发布</b></li>
                  </ul>
                </div>
            </div>
            </div>
      );
  }
}

export default App;
