import React, { Component } from 'react';
import './default.scss';
class App extends Component {
  render() {
    return (
            <div className="groom-main-center-top-default">
             <div className="img-box">
                <textarea className="img-box-top" placeholder="有什么新鲜事告诉大家"></textarea>
                <p className="img-box-word">0/2000字</p>
                <div className="img-box-bottom">
                  <ul className="img-box-bottom-ul">
                    <li><i className="iconfont icon-tupian"></i><span>添加图片</span></li>
                    <li><i className="iconfont icon-wenzhang1"></i><span>发布长文</span></li>
                    <li><i className="iconfont icon-Smile"></i></li>
                    <li className="img-box-bottom-ul-lilast"><b>发布</b></li>
                  </ul>
                </div>
            </div>
            </div>
      );
  }
}

export default App;
