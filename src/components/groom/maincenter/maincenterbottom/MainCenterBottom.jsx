import React, { Component } from 'react';
import './maincenterbottom.scss';
class App extends Component {
  render() {
    return (
          <div className="groom-main-center-bottom">
          <ul className="groom-main-center-bottom-content">
            <li className="groom-main-center-bottom-content-li">
              <div className="content-left">
              <a className="content-left-img" href="/"><img src="https://p3.pstatp.com/list/190x124/pgc-image/e328868620c04127be28d22283cc7916" alt=""/>
              </a>
              </div>
              <div className="content-right">
                <div className="content-right-title">
                 <a href="/" className="content-right-title-a">1952年，刘少奇访苏，他是斯大林最后一次会见的中国领导人，斯大林当面给予重要建议</a>
                </div>
                <div className="content-right-other">
                  <a href="/" className="content-right-other-kind">社会</a>
                  <a href="/" className="content-right-other-origin"><img src="//p2.pstatp.com/large/78c10010fa08f0762b97" alt=""/></a>
                  <a href="/" className="content-right-other-name">
                  语录书黑
                  </a>
                  <a href="/" className="content-right-other-common">评论</a>
                  <span className="content-right-other-time">四小时前</span>
                  <a href="/" className="content-right-other-button iconfont icon-xhao"> </a>
                </div>
              </div>
            </li>
          </ul>
          </div>
      );
  }
}

export default App;
