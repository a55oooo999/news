import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './todaylogo.scss';
class App extends Component {
  render() {
    return (
          <div className="today-logo">
           <Link className="today-logo-box" to="/groom"><img className="today-logo-img" src="https://s3.pstatp.com/toutiao/static/img/logo.201f80d.png" alt=""/>
           </Link>
          </div>
      );
  }
}

export default App;
