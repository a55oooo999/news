import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './globalnav.scss';
class App extends Component {
  render() {
    return (
        <ul>
            <li><img src="//s3.pstatp.com/toutiao/static/img/logo.201f80d.png" alt=""/></li>
            <li><Link to="/groom">推荐</Link></li>
            <li><Link to="/sun">阳光宽频</Link></li>
            <li><Link to="/hotspot">热点</Link></li>
            <li><Link to="/photo">图片</Link></li>
            <li><Link to="/science">科技</Link></li>
            <li><Link to="/joy">娱乐</Link></li>
            <li><Link to="/game">游戏</Link></li>
            <li><Link to="/sports">体育</Link></li>
            <li><Link to="/car">汽车</Link></li>
            <li><Link to="/finance">财经</Link></li>
            <li><Link to="/funny">搞笑</Link></li>
            <li><Link to="/more">更多</Link></li>
        </ul>
    );
  }
}

export default App;
