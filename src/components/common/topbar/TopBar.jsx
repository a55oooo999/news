import React, { Component } from 'react';
import './topbar.scss';
class App extends Component {
  render() {
    return (
      <header>
        <div className="topbar">
      <ul className="topbar-left">
        <li>
          <a href="/"><span>下载App</span></a>
        </li>
        <li>
          <a href="/"><span>北京</span><span>晴</span><span>-12</span><span>/-1</span></a>
        </li>
      </ul>
      <ul className="topbar-right">
        <li><a href="/">用户名</a></li>
        <li><a href="/">反馈</a></li>
        <li><a href="/">投诉侵权</a></li>
        <li><a href="/">头条产品</a></li>
      </ul>
      </div>
      </header>
    );
  }
}

export default App;
