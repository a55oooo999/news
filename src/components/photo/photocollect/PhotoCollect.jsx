import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './photocollect.scss';
class App extends Component {
  render() {
    return (
      <div className="photo-main-center-collect">
      <ul className="photo-main-center-collect-box">
              <li className="photo-main-center-collect-box-item">
                <div className="collect-box-item">
                  <div className="collect-box-item-imgbox">
                  <a href="/" className="collect-box-item-imgbox-img"> 
                  <img src="https://p3.pstatp.com/list/640x360/pgc-image/1540658913817c42a6c2830" alt="" />
                  </a>
                  <span className="collect-box-item-imgbox-num">6图</span>
                  </div>
              <div className="collect-box-item-imgbox-info">
                <p className="collect-box-item-imgbox-info-top"><a href="/">西部地区最大：成都双流机场2018旅客吞吐量破5千万</a></p>
                <p className="collect-box-item-imgbox-info-bottom">
                 <Link to="/" className="collect-box-item-imgbox-info-bottom-origin">轨道上的江苏.</Link>
                 <Link to="/" className="collect-box-item-imgbox-info-bottom-commont">23评论</Link>
                </p>
              </div>
              </div>
              </li>
            </ul>
      </div>
      );
  }
}

export default App;
