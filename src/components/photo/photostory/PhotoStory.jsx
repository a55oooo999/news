import React, { Component } from 'react';
import './photostory.scss';
class App extends Component {
  render() {
    return (
      <div className="photo-main-center-story">
      <ul className="photo-main-center-story-box">
              <li className="photo-main-center-story-box-item">
                <div className="story-box-item">
                  <div className="story-box-item-imgbox">
                  <a className="story-box-item-imgbox-img" href="/">
                  <img src="https://p1.pstatp.com/list/640x360/pgc-image/54cf3e9f32b54b669f3c5a7619785d76" alt="" />
                  </a>
                  <span className="story-box-item-imgbox-num">6图</span>
                  </div>
              <div className="story-box-item-imgbox-info">
                <p className="story-box-item-imgbox-info-top"><a href="/">西部地区最大：成都双流机场2018旅客吞吐量破5千万</a></p>
                <p className="story-box-item-imgbox-info-bottom">
                 <a href="/" className="story-box-item-imgbox-info-bottom-origin">轨道上的江苏.</a>
                 <a href="/" className="story-box-item-imgbox-info-bottom-commont">23评论</a>
                </p>
              </div>
              </div>
              </li>
            </ul>
      </div>
      );
  }
}

export default App;
