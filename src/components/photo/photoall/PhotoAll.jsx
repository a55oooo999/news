import React, { Component } from 'react';
import './photoall.scss';
class App extends Component {
  render() {
    return (
          <div className="photo-main-center-all">
            <ul className="photo-main-center-all-box">
              <li className="photo-main-center-all-box-item">
                <div className="all-box-item">
                  <div className="all-box-item-imgbox">
                  <a className="all-box-item-imgbox-img" href="/"><img src="https://p1.pstatp.com/list/640x360/pgc-image/32fdc6344b244e4fadda6271db76eb43" alt="" />
                  </a>
                  <span className="all-box-item-imgbox-num">6图</span>
                  </div>
              <div className="all-box-item-imgbox-info">
                <p className="all-box-item-imgbox-info-top"><a href="/">西部地区最大：成都双流机场2018旅客吞吐量破5千万</a></p>
                <p className="all-box-item-imgbox-info-bottom">
                 <a href="/" className="all-box-item-imgbox-info-bottom-origin">轨道上的江苏.</a>
                 <a href="/" className="all-box-item-imgbox-info-bottom-commont">23评论</a>
                </p>
              </div>
              </div>
              </li>
            </ul>
          </div>
      );
  }
}

export default App;
