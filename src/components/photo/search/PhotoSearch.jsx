import React, { Component } from 'react';
import './photosearch.scss';
class App extends Component {
  render() {
    return (
       <div className="photo-search">
       <form>
         <div className="photo-search-left">
          <input type="search" placeholder="请输入关键字" />
         </div>
         <div className="photo-search-right">
         <button type="submit">
         <span className="iconfont icon-iconfontsousuo"></span>
         </button>
         </div>
       </form>
        </div>
    )
  }
}

export default App;
