import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './headernav.scss';
class App extends Component {
  render() {
    return (
       <div className="photoheader">
        <div className="headernav">
        <ul className="headernav-left">
            <li><Link to="/groom">推荐</Link></li>
            <li><Link to="/hotspot">热点</Link></li>
            <li><Link to="/sun">视频</Link></li>
            <li><Link to="/photo">图片</Link></li>
            <li><Link to="/joy">娱乐</Link></li>
            <li><Link to="/science">科技</Link></li>
            <li><Link to="/game">游戏</Link></li>
            <li><Link to="/sports">体育</Link></li>
            <li><Link to="/car">汽车</Link></li>
            <li><Link to="/finance">财经</Link></li>
            <li><Link to="/funny">搞笑</Link></li>
            <li><Link to="/more">更多</Link></li>
        </ul>
        <ul className="headernav-right">
        <li><a href="/">用户名</a></li>
        <li><a href="/">反馈</a></li>
        <li><a href="/">投诉侵权</a></li>
        <li><a href="/">头条产品</a></li>
      </ul>
        </div>
        </div>
    );
  }
}

export default App;
