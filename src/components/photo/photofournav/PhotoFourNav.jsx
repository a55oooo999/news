import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './photofournav.scss';
class App extends Component {
  componentDidMount () {
    console.log(this.props.list)
  }
  render() {
    return (
        <ul className="photo-four-nav">
          {
            this.props.list.map((item,index)=>{
            return (
            <li  key={index}>
              <NavLink to={item.path}>{item.name}</NavLink>
            </li>
            )
            })
          }
        </ul>
    );
  }
}

export default App;
