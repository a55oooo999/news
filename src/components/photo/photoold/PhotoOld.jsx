import React, { Component } from 'react';
import './photoold.scss';
class App extends Component {
  render() {
    return (
      <div className="photo-main-center-old">
      <ul className="photo-main-center-old-box">
              <li className="photo-main-center-old-box-item">
                <div className="old-box-item">
                  <div className="old-box-item-imgbox">
                  <a href="/" className="old-box-item-imgbox-img">
                  <img src="https://p3.pstatp.com/list/640x360/pgc-image/775da2e2db71490baa89eea6ea86e7c8" alt="" />
                  </a>
                  <span className="old-box-item-imgbox-num">6图</span>
                  </div>
              <div className="old-box-item-imgbox-info">
                <p className="old-box-item-imgbox-info-top"><a href="/">西部地区最大：成都双流机场2018旅客吞吐量破5千万</a></p>
                <p className="old-box-item-imgbox-info-bottom">
                 <a href="/" className="old-box-item-imgbox-info-bottom-origin">轨道上的江苏.</a>
                 <a href="/" className="old-box-item-imgbox-info-bottom-commont">23评论</a>
                </p>
              </div>
              </div>
              </li>
            </ul>
      </div>
      );
  }
}

export default App;
