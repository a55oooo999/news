import React, { Component } from 'react';
import { Route, Switch, Redirect, Link} from 'react-router-dom';
import HeaderNav from '@/components/photo/HeaderNav';
import PhotoSearch from '@/components/photo/search/PhotoSearch';
import PhotoFourNav from '@/components/photo/photofournav/PhotoFourNav';
import PhotoAll from '@/components/photo/photoall/PhotoAll';
import PhotoStory from '@/components/photo/photostory/PhotoStory';
import PhotoCollect from '@/components/photo/photocollect/PhotoCollect';
import PhotoOld from '@/components/photo/photoold/PhotoOld';
import './photomain.scss';
class App extends Component {
  constructor(props){
    super(props);
      this.state={
        list:[
          {
            path:'/photo/all',
            name:'全部'
          },
          {
            path:'/photo/old',
            name:'老照片'
          },
          {
            path:'/photo/story',
            name:'图片故事'
          },
          {
            path:'/photo/collect',
            name:'摄影集'
          }
        ]
      }
    }
  render() {
        return (
          <div className="container">
          <HeaderNav />
          <div className="photo-main">
          <div className="photo-logo">
            <ul>
              <li>
                <Link to="/groom"><img src="https://s3.pstatp.com/toutiao/resource/ntoutiao_web/static/image/logo_201f80d.png" alt="" />
                </Link>
              </li>
              <li className="second">
                <PhotoSearch />
              </li>
            </ul>
          </div>
          <div className="photo-main-center">
          <PhotoFourNav list={this.state.list} />
          <Switch>
          <Route path="/photo/all" component = { PhotoAll } />
          <Route path="/photo/old" component = { PhotoOld } />
          <Route path="/photo/story" component = { PhotoStory } />
          <Route path="/photo/collect" component = { PhotoCollect } />
          <Redirect from='/photo' to='/photo/all'/>
        </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
