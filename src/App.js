import React, { Component } from 'react';
import TopBar from '@/components/common/topbar/TopBar';
import GlobalNav from '@/components/common/globalnav/GlobalNav';
class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="header">
         <TopBar/>
        </div>
        <div className="main">
          <div className="main-left">
          <GlobalNav />
          </div>
          测试
          <div className="main-center">
          </div>
          <div className="main-right"></div>
        </div>
      </div>
    );
  }
}

export default App;
